package fileSender.communication.model;

import java.io.Serializable;

public class ServerConfirmation extends Message implements Serializable {
    private static final long serialVersionUID = 3L;
    private int port;

    public ServerConfirmation(TYPE type, String requestId) {
        super(type, requestId);
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
