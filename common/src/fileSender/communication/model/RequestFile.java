package fileSender.communication.model;

import java.io.Serializable;

public class RequestFile implements Serializable{
    private static final long serialVersionUID = 2L;

    String fileName;
    String requestId;
    String fileHash;

    public String getFileName() {
        return fileName;
    }

    public String getRequestId() {
        return requestId;
    }

    public String getFileHash() {
        return fileHash;
    }
}
