package fileSender.communication.model;

import java.io.Serializable;

public class ClientInitRequest extends Message implements Serializable {
    private static final long serialVersionUID = 4L;

    public ClientInitRequest(TYPE type, String requestId) {
        super(type, requestId);
    }
}
