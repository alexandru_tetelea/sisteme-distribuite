package fileSender.communication.model;

public class ServerInitResponse extends Message {
    private final int port;

    public int getPort() {
        return port;
    }

    public ServerInitResponse(TYPE type, String requestId, int port) {
        super(type, requestId);
        this.port = port;
    }
}
