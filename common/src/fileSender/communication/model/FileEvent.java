package fileSender.communication.model;

import java.io.Serializable;

public class FileEvent implements Serializable, Comparable {

    private static final long serialVersionUID = 1L;
    private String filename;
    private long fileSize;
    private byte[] fileData;
    private String status;
    private long id;

    public FileEvent() {
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public byte[] getFileData() {
        return fileData;
    }

    public void setFileData(byte[] fileData) {
        this.fileData = fileData;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public int compareTo(Object o) {
        return Long.compare(this.id, ((FileEvent) o).id);
    }
}


