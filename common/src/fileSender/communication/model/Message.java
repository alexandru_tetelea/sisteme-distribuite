package fileSender.communication.model;

import java.io.Serializable;

public class Message implements Serializable {
    private static final long serialVersionUID = 10L;

    private TYPE type;
    private String requestId;

    public Message(TYPE type, String requestId) {
        this.type = type;
        this.requestId = requestId;
    }

    public TYPE getType() {
        return type;
    }

    public void setType(TYPE type) {
        this.type = type;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public enum TYPE {
        ERROR,
        INIT_CONNECTION,
        CONFIRM_CONNECTION,
        REJECT_CONNECTION,
        STOP_TRANSFER,
        CONTINUE_TRANSFER
    }
}
