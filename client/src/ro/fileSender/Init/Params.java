package ro.fileSender.Init;

import java.util.Scanner;
import java.util.stream.Stream;

public class Params {
    public static Operation getOperation() {
        Operation operation = new Operation();
        Scanner sc = new Scanner(System.in);
        operation.setOperationType(getOperationType(sc));
        operation.setFileName(getFileName(sc));
        return operation;
    }

    private static Operation.Type getOperationType(Scanner scanner) {
        Integer typeInt;
        while (true) {
            System.out.println("Choose communication protocol:\n1. UDP\n2. TCP");
            typeInt = scanner.nextInt();

            if (typeInt < 0 || typeInt > Operation.Type.values().length) {
                System.out.println("Ati gresit tipul operatiei!");
                continue;
            }
            break;
        }
        final int finalInt = typeInt;
        return Stream.of(Operation.Type.values()).filter(it -> it.getTypeInt() == finalInt).findFirst().orElse(null);
    }

    private static String getFileName(Scanner scanner) {
        System.out.println("Specify the file name:");
        String fileName = scanner.next();
        return fileName;
    }
}
