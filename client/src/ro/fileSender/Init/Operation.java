package ro.fileSender.Init;

public class Operation {
    private Type operationType;
    private String fileName;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Type getOperationType() {
        return operationType;
    }

    public void setOperationType(Type operationType) {
        this.operationType = operationType;
    }

    public static enum Type {
        UDP(1),
        TCP(2);

        private final int typeInt;

        Type(int typeInt) {
            this.typeInt = typeInt;
        }

        public int getTypeInt() {
            return typeInt;
        }
    }
}
