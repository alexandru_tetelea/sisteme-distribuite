package ro.fileSender.communication;

import fileSender.communication.model.ClientInitRequest;
import fileSender.communication.model.FileEvent;
import fileSender.communication.model.ServerInitResponse;

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static fileSender.communication.Configuration.SERVER_PORT;
import static fileSender.communication.model.Message.TYPE.CONFIRM_CONNECTION;
import static fileSender.communication.model.Message.TYPE.INIT_CONNECTION;
import static java.lang.String.*;
import static java.util.Optional.empty;
import static java.util.Optional.of;

public class UdpClient implements Client {
    private final int READ_SIZE = 51200;
    private String hostName = "18.197.115.76";

    private Optional<ServerInitResponse> getServerStatus() {
        try {
            DatagramSocket socket = new DatagramSocket();
            while (true) {
                //Ask Server about permision
                InetAddress IPAddress = InetAddress.getByName(hostName);
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                ObjectOutputStream os = new ObjectOutputStream(outputStream);

                ClientInitRequest confirmation = new ClientInitRequest(INIT_CONNECTION, UUID.randomUUID().toString());
                os.writeObject(confirmation);
                byte[] data = outputStream.toByteArray();
                DatagramPacket sendPacket = new DatagramPacket(data, data.length, IPAddress, SERVER_PORT);
                socket.send(sendPacket);

                //Receive Server permision with new port
                byte[] incomingData = new byte[1024 * 1000 * 50];
                DatagramPacket incomingPacket = new DatagramPacket(incomingData, incomingData.length);
                socket.receive(incomingPacket);
                byte[] receivedData = incomingPacket.getData();

                ByteArrayInputStream in = new ByteArrayInputStream(receivedData);
                ObjectInputStream is = new ObjectInputStream(in);
                ServerInitResponse serverInitResponse = (ServerInitResponse) is.readObject();

                return of(serverInitResponse);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return empty();
    }

    public void sendFile(String sourceFilePath, int port) {
        try {
            Thread.sleep(1000);
            DatagramSocket socket = new DatagramSocket();
            InetAddress IPAddress = InetAddress.getByName(hostName);
            List<FileEvent> events = getFileEvent(sourceFilePath);
            int index = 0;
            for (FileEvent event : events) {
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                ObjectOutputStream os = new ObjectOutputStream(outputStream);
                os.writeObject(event);
                byte[] data = outputStream.toByteArray();
                DatagramPacket sendPacket = new DatagramPacket(data, data.length, IPAddress, port);
                socket.send(sendPacket);
                receiveConfirmation(socket, event.getId());
                index++;
                System.out.println(index);
            }
            System.out.println(index);
            Thread.sleep(2000);
            System.exit(0);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private boolean receiveConfirmation(DatagramSocket socket, long id) throws IOException {
        for (int i = 0; i < 20; i++) {
            byte[] incomingData = new byte[READ_SIZE];
            DatagramPacket incomingPacket = new DatagramPacket(incomingData, incomingData.length);
            socket.receive(incomingPacket);
            String response = new String(incomingPacket.getData());
            if (response.equals("STOP")) {
                return true;
            }
            if (response.trim().equals(valueOf(id))) {
                return true;
            }
        }
        System.exit(0);
        return true;
    }

    public List<FileEvent> getFileEvent(String sourceFilePath) {
        FileEvent fileEvent = null;
        List<FileEvent> events = new ArrayList<>();
        String fileName = sourceFilePath.substring(sourceFilePath.lastIndexOf(File.separator) + 1, sourceFilePath.length());
        String path = sourceFilePath.substring(0, sourceFilePath.lastIndexOf(File.separator) + 1);
        File file = new File(sourceFilePath);
        if (file.isFile()) {
            try {
                DataInputStream diStream = new DataInputStream(new FileInputStream(file));
                long len = (int) file.length();
                int size = READ_SIZE > len ? (int) len : READ_SIZE;
                int id = 0;
                long globalRead = 0;
                while (true) {
                    byte[] fileBytes = new byte[READ_SIZE];
                    int read = 0;

                    int numRead = 0;
                    while (read < fileBytes.length && (numRead = diStream.read(fileBytes, read, size)) >= 0) {
                        size = len - globalRead >= size ? size : (int) (size - numRead);
                        read = read + numRead;
                        globalRead = globalRead + numRead;
                    }

                    fileEvent = new FileEvent();
                    fileEvent.setFilename(fileName);
                    fileEvent.setFileSize(len);
                    fileEvent.setFileData(fileBytes);
                    fileEvent.setStatus("Success");
                    fileEvent.setId(++id);
                    events.add(fileEvent);

                    if (numRead == -1) {
                        fileEvent.setStatus("END");
                        break;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                fileEvent.setStatus("Error");
            }
        } else {
            System.out.println("path specified is not pointing to a file");
            fileEvent.setStatus("Error");
        }
        return events;
    }

    @Override
    public void send(String fileName) {
        getServerStatus()
                .filter(it -> it.getType().equals(CONFIRM_CONNECTION))
                .ifPresent(it -> sendFile(fileName, it.getPort()));
    }
}
