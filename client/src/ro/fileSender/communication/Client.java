package ro.fileSender.communication;

public interface Client {
    void send(String fileName);
}
