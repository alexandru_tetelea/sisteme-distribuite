package ro.fileSender;

import fileSender.communication.Configuration;
import ro.fileSender.communication.UdpServer;

public class Run {
    public static void main(String[] args) {
        UdpServer.run(Configuration.SERVER_PORT);
    }
}
