package ro.fileSender.communication;

import fileSender.communication.model.ClientInitRequest;
import fileSender.communication.model.FileEvent;
import fileSender.communication.model.ServerInitResponse;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static fileSender.communication.Configuration.SERVER_PORT;
import static fileSender.communication.model.Message.TYPE.CONFIRM_CONNECTION;
import static fileSender.communication.model.Message.TYPE.INIT_CONNECTION;
import static java.lang.String.valueOf;
import static java.lang.System.*;
import static java.util.Optional.empty;
import static java.util.Optional.of;

public class UdpServer {

    private final int READ_SIZE = 1024;
    private final int serverPort;
    private String hostName = "localHost";

    private UdpServer(int serverPort) {
        this.serverPort = serverPort;
    }

    public static void run(int port) {
        System.out.println("Server is running at the port: " + port);
        new UdpServer(port).start();
    }

    private void start() {
        String destinationPath = "C:\\Users\\atetelea\\files\\dst\\";

        try {
            initConnectionWithClient()
                    .filter(it -> it.getType().equals(CONFIRM_CONNECTION))
                    .ifPresent(it -> receiveFile(it.getRequestId(), it.getPort(), destinationPath));
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private Optional<ServerInitResponse> initConnectionWithClient() throws IOException, ClassNotFoundException {
        //wait client communication
        DatagramSocket socket = new DatagramSocket(SERVER_PORT);
        byte[] incomingData = new byte[READ_SIZE * 1000 * 50];
        DatagramPacket incomingPacket = new DatagramPacket(incomingData, incomingData.length);
        socket.receive(incomingPacket);
        byte[] data = incomingPacket.getData();
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        ObjectInputStream is = new ObjectInputStream(in);
        ClientInitRequest clientInitRequest = (ClientInitRequest) is.readObject();

        if (!clientInitRequest.getType().equals(INIT_CONNECTION)) {
            return empty();
        }
        //Server response with new communication port
        InetAddress IPAddress = incomingPacket.getAddress();
        int port = incomingPacket.getPort();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ObjectOutputStream os = new ObjectOutputStream(outputStream);
        ServerInitResponse confirmation = new ServerInitResponse(CONFIRM_CONNECTION, clientInitRequest.getRequestId(), new ServerSocket(0).getLocalPort());
        os.writeObject(confirmation);
        byte[] responseData = outputStream.toByteArray();
        DatagramPacket sendPacket = new DatagramPacket(responseData, responseData.length, IPAddress, port);
        socket.send(sendPacket);

        return of(confirmation);
    }

    private void receiveFile(String communicationId, int port, String destinationPath) {
        try {
            DatagramSocket socket = new DatagramSocket(port);
            List<FileEvent> fileEvents = new ArrayList<>();
            long executionTime = 0;
            long startTime = 0;
            long packetReceived = 0;
            long bytesReceived = 0;
            while (true) {
                startTime = currentTimeMillis();

                byte[] incomingData = new byte[READ_SIZE * 1000 * 50];
                DatagramPacket incomingPacket = new DatagramPacket(incomingData, incomingData.length);
                socket.receive(incomingPacket);
                byte[] data = incomingPacket.getData();

                ByteArrayInputStream in = new ByteArrayInputStream(data);
                ObjectInputStream is = new ObjectInputStream(in);
                FileEvent fileEvent = (FileEvent) is.readObject();
                if (fileEvent.getStatus().equalsIgnoreCase("Error")) {
                    out.println("Some issue happened while packing the data @ client side");
                    exit(0);
                }
                fileEvents.add(fileEvent);

                confirmReceived(incomingPacket, fileEvent, socket);

                executionTime += currentTimeMillis() - startTime;
                packetReceived++;
                bytesReceived += data.length;
                if (fileEvent.getStatus().equalsIgnoreCase("End")) {
                    out.println("Last Packet");
                    break;
                }
            }

            createAndWriteFile(fileEvents, destinationPath); // writing the file to hard disk

            out.println("Packet Received: " + packetReceived);
            out.println("Bytes Received: " + bytesReceived);
            out.println("Execution Time: " + executionTime);
            Thread.sleep(3000);
            exit(0);
        } catch (IOException | ClassNotFoundException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void confirmReceived(DatagramPacket incomingPacket, FileEvent fileEvent, DatagramSocket socket) throws IOException {
        InetAddress IPAddress = incomingPacket.getAddress();
        int port = incomingPacket.getPort();
        String reply = valueOf(fileEvent.getId());
        byte[] replyBytes = reply.getBytes();
        DatagramPacket replyPacket = new DatagramPacket(replyBytes, replyBytes.length, IPAddress, port);
        socket.send(replyPacket);
    }

    private void createAndWriteFile(List<FileEvent> fileEvents, String destinationPath) {
        Collections.sort(fileEvents);
        String outputFile = destinationPath + fileEvents.get(0).getFilename();
        if (!new File(destinationPath).exists()) {
            new File(destinationPath).mkdirs();
        }
        File dstFile = new File(outputFile);

        try (FileOutputStream fileOutputStream = new FileOutputStream(dstFile)) {
            for (FileEvent fileEvent : fileEvents) {
                fileOutputStream.write(fileEvent.getFileData());
            }
            fileOutputStream.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
        out.println("Output file : " + outputFile + " is successfully saved ");
    }

    private void requestFile() throws SocketException, UnknownHostException {
        DatagramSocket socket = new DatagramSocket();
        InetAddress IPAddress = InetAddress.getByName(hostName);
    }
}
